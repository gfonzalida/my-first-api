package my.api.example.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class SalidaController {
    @PostMapping("/salida")
    public String Salida(){
        return "Método Salida de tipo POST de mi API";
    }
}
