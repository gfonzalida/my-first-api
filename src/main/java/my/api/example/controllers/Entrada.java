package my.api.example.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Entrada {
    @RequestMapping("/entrada")
    public String Entrada(){
        return "Recurso 'Entrada' de mi API con Método GET";
    }
}
