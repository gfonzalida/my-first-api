package my.api.example.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {
    @RequestMapping("/")
    public String Home(){
        return "Bienvenido a mi primer API Java Spring";
    }
}
