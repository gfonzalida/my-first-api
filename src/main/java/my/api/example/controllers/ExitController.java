package my.api.example.controllers;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExitController {
    @PostMapping ("/")
    public String Exit(){
        return "Método POST de mi API de Java Spring";
    }


}
